/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.lania.ejemplopermisos.entidades;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author jaguilar
 */
@Entity
@Table(name = "cambios_estado_permiso")
public class CambioEstadoPermiso implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_cambio_estado")
    private Integer idCambioEstado;
    @Column(name = "fecha")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fecha;
    @JoinColumn(name = "id_estado_permiso", referencedColumnName = "id_estado_permiso")
    @ManyToOne(optional = false)
    private EstadoPermiso idEstadoPermiso;
    @JoinColumn(name = "id_permiso", referencedColumnName = "id_permiso")
    @ManyToOne(optional = false)
    private Permiso idPermiso;
    @JoinColumn(name = "id_usuario", referencedColumnName = "id_usuario")
    @ManyToOne(optional = false)
    private Usuario idUsuario;

    public CambioEstadoPermiso() {
    }

    public CambioEstadoPermiso(Integer idCambioEstado) {
        this.idCambioEstado = idCambioEstado;
    }

    public Integer getIdCambioEstado() {
        return idCambioEstado;
    }

    public void setIdCambioEstado(Integer idCambioEstado) {
        this.idCambioEstado = idCambioEstado;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public EstadoPermiso getIdEstadoPermiso() {
        return idEstadoPermiso;
    }

    public void setIdEstadoPermiso(EstadoPermiso idEstadoPermiso) {
        this.idEstadoPermiso = idEstadoPermiso;
    }

    public Permiso getIdPermiso() {
        return idPermiso;
    }

    public void setIdPermiso(Permiso idPermiso) {
        this.idPermiso = idPermiso;
    }

    public Usuario getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(Usuario idUsuario) {
        this.idUsuario = idUsuario;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idCambioEstado != null ? idCambioEstado.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CambioEstadoPermiso)) {
            return false;
        }
        CambioEstadoPermiso other = (CambioEstadoPermiso) object;
        if ((this.idCambioEstado == null && other.idCambioEstado != null) || (this.idCambioEstado != null && !this.idCambioEstado.equals(other.idCambioEstado))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mx.lania.ejemplopermisos.entidades.CambioEstadoPermiso[ idCambioEstado=" + idCambioEstado + " ]";
    }
    
}
