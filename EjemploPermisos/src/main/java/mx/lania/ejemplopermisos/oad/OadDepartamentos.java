package mx.lania.ejemplopermisos.oad;

import java.util.List;
import mx.lania.ejemplopermisos.entidades.Departamento;

/**
 *
 * @author jaguilar
 */
public interface OadDepartamentos {
    
    void crear(Departamento depto);
    
    void actualizar(Departamento depto);
    
    void activar(Integer idDepto);
    void desactivar(Integer idDepto);
    
    List<Departamento> buscarActivosPorNombre(String nombre);

    List<Departamento> buscarPorNombre(String nombre);
    
    List<Departamento> getActivos();
    
    Departamento getPorId(Integer id);
    
}
