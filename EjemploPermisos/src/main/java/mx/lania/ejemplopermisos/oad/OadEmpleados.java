package mx.lania.ejemplopermisos.oad;

import java.util.List;
import mx.lania.ejemplopermisos.entidades.Empleado;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 *
 * @author PCEL
 */
public interface OadEmpleados extends JpaRepository<Empleado, Integer> {
    
    @Query("SELECT e FROM Empleado e WHERE e.departamento.idDepto = :idDepto")
    List<Empleado> getPorIdDepartamento(@Param("idDepto") Integer idDepartamento);

    public List<Empleado> findByNombreEmpleado(String nombre);
}
