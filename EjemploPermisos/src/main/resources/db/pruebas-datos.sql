INSERT INTO departamento(id_depto, nombre_depto) VALUES (1,'Administración');
INSERT INTO departamento(id_depto, nombre_depto) VALUES (2,'Consultoría');

INSERT INTO tipo_contrato(id_tipo_contrato,nombre_contrato) VALUES (1, 'Base');
INSERT INTO tipo_contrato(id_tipo_contrato,nombre_contrato) VALUES (2, 'Proyecto');
INSERT INTO tipo_contrato(id_tipo_contrato,nombre_contrato) VALUES (3, 'Interino');

INSERT INTO empleado(id_empleado,nombre_empleado, apellido_paterno,id_tipo_contrato, id_depto) VALUES (1,'Hugo', 'Hernández', 1, 1);
INSERT INTO empleado(id_empleado,nombre_empleado, apellido_paterno,id_tipo_contrato, id_depto) VALUES (2,'Paco', 'Perez', 1, 2);
INSERT INTO empleado(id_empleado,nombre_empleado, apellido_paterno,id_tipo_contrato, id_depto) VALUES (3,'Luis', 'López', 2, 2);

